The defense attorneys at The Grieco Criminal Law Center are experienced, passionate and aggressive and have built a reputation for providing superior representation to all of our clients in Miami Dade County, Broward County, and throughout the State of Florida.

Address: 1688 Meridian Ave, #900, Miami Beach, FL 33139, USA
Phone: 305-857-0034
